.PHONY: setup init measure clean

export EASYRSA=keys/easyrsa
export EASYRSA_VARS_FILE=keys/easyrsa/vars

setup:
	pacman -Syu --needed iperf3 netperf easy-rsa openvpn python-seaborn python-matplotlib python-scipy python-gitpython

init:
	mkdir -p keys/easyrsa
	cp /etc/easy-rsa/* keys/easyrsa -R 
	easyrsa init-pki
	easyrsa build-ca
	easyrsa gen-req server nopass
	easyrsa gen-req client nopass
	openvpn --genkey secret keys/openvpn_ta.key
	openvpn --genkey secret keys/openvpn_psk
	easyrsa sign-req server server
	easyrsa sign-req client client

measure:
	./measure singlehop local    direct      2>&1 | tee -a log/singlehop_measure.log
	./measure singlehop local    wg          2>&1 | tee -a log/singlehop_measure.log
	./measure singlehop local    openvpn_tls 2>&1 | tee -a log/singlehop_measure.log
	./measure singlehop internet direct      2>&1 | tee -a log/singlehop_measure.log
	./measure singlehop internet wg          2>&1 | tee -a log/singlehop_measure.log
	./measure singlehop internet openvpn_tls 2>&1 | tee -a log/singlehop_measure.log
	./measure threehop  local    direct      2>&1 | tee -a log/threehop_measure.log
	./measure threehop  local    wg          2>&1 | tee -a log/threehop_measure.log
	./measure threehop  local    openvpn_tls 2>&1 | tee -a log/threehop_measure.log
	./measure threehop  internet direct      2>&1 | tee -a log/threehop_measure.log
	./measure threehop  internet wg          2>&1 | tee -a log/threehop_measure.log
	./measure threehop  internet openvpn_tls 2>&1 | tee -a log/threehop_measure.log

clean:
	rm -rf keys/
	git clean -fdX
