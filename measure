#!/bin/sh
function usage {
    echo "measure (singlehop|threehop) (local|internet) (direct|wg|openvpn_psk|openvpn_tls|openvpn_none)"
}

export PATH=`pwd`/scripts:${PATH}

HOPS=$1
CONDITION=$2
PROTOCOL=$3

if [[ "${HOPS}" = "singlehop" ]]; then
    NS_SERVER=EP1
    NS_CLIENT=N2
elif [[ "${HOPS}" = "threehop" ]]; then
    NS_SERVER=EP1
    NS_CLIENT=EP2
else
    usage
    exit 1
fi

if [[ "${CONDITION}" = "local" ]]; then
    echo 'local conditions'
elif [[ "${CONDITION}" = "internet" ]]; then
    echo 'internet conditions'
else
    usage
    exit 1
fi

if [[ "${PROTOCOL}" = "direct" ]]; then
    IP_SERVER=1.1.1.2
    DEV_LEFT=
    DEV_RIGHT=
elif [[ "${PROTOCOL}" = "wg" ]]; then
    IP_SERVER=11.0.0.1
    DEV_LEFT=wgl
    DEV_RIGHT=wgr
elif [[ "${PROTOCOL}" = "openvpn_psk" ]]; then
    IP_SERVER=11.0.0.1
    DEV_LEFT=tun1
    DEV_RIGHT=tun2
elif [[ "${PROTOCOL}" = "openvpn_tls" ]]; then
    IP_SERVER=11.0.0.1
    DEV_LEFT=tun1
    DEV_RIGHT=tun2
elif [[ "${PROTOCOL}" = "openvpn_none" ]]; then
    IP_SERVER=11.0.0.1
    DEV_LEFT=tun1
    DEV_RIGHT=tun2
else
    usage
    exit 1
fi

mkdir -p log/
LOG_PREFIX=log/${HOPS}_${CONDITION}_${PROTOCOL}_

if [ "${DEBUG}" = "1" ]; then
    TIME=5
else
    TIME=600
    rm ${LOG_PREFIX}*
    log_metadata ${LOG_PREFIX}
fi

mkdir -p .tmp
touch .tmp/pids

set -ex

ip netns add N1
ip netns add N2
ip netns add N3
ip netns add EP1
ip netns add EP2

function cleanup() {
    if [ "$(< .tmp/pids)" != "" ]; then
        kill $(cat .tmp/pids)
    fi

    rm -rf .tmp/

    ip netns del EP1
    ip netns del N1
    ip netns del N2
    ip netns del N3
    ip netns del EP2
}

trap cleanup EXIT

ip link add veth1l type veth peer name veth1r
ip link add veth2l type veth peer name veth2r
ip link add veth3l type veth peer name veth3r
ip link add veth4l type veth peer name veth4r

ip link set veth1l netns EP1
ip link set veth1r netns N1

ip link set veth2l netns N1
ip link set veth2r netns N2

ip link set veth3l netns N2
ip link set veth3r netns N3

ip link set veth4l netns N3
ip link set veth4r netns EP2

ip netns exec EP1 ip link set lo up
ip netns exec EP1 ip link set veth1l up
ip netns exec EP1 ip addr add 1.1.1.2/24 dev veth1l
ip netns exec EP1 tc qdisc add dev veth1l root tbf rate 1000mbit burst 1000kb latency 100ms
if [ "${PROTOCOL}" != "direct" ]; then
    ip netns exec EP1 left_${PROTOCOL} ${LOG_PREFIX} 11.0.0.1 1.1.1.1
    ip netns exec EP1 ip route add 0.0.0.0/0 dev ${DEV_LEFT}
else
    ip netns exec EP1 ip route add 1.1.0.0/16 via 1.1.1.1
fi

ip netns exec N1 ip link set lo up
ip netns exec N1 ip link set veth1r up
ip netns exec N1 ip link set veth2l up
ip netns exec N1 ip addr add 1.1.1.1/24 dev veth1r
ip netns exec N1 ip addr add 1.1.2.2/24 dev veth2l
if [ "${CONDITION}" = "internet" ]; then
    ip netns exec N1 tc qdisc add dev veth1r root netem delay 15ms 0.5ms 25% distribution normal loss 0.5% 25%
    ip netns exec N1 tc qdisc add dev veth2l root netem delay 15ms 0.5ms 25% distribution normal loss 0.5% 25%
else
    ip netns exec N1 tc qdisc add dev veth1r root tbf rate 1000mbit burst 1000kb latency 1ms
    ip netns exec N1 tc qdisc add dev veth2l root tbf rate 1000mbit burst 1000kb latency 1ms
fi
ip netns exec N1 echo 1 > /proc/sys/net/ipv4/ip_forward
if [ "${PROTOCOL}" != "direct" ]; then
    ip netns exec N1 right_${PROTOCOL} ${LOG_PREFIX} 11.0.0.2 1.1.1.2
    ip netns exec N1 left_${PROTOCOL} ${LOG_PREFIX} 11.0.1.3 1.1.2.1
    ip netns exec N1 ip route add 11.0.0.0/24 dev ${DEV_RIGHT}
    ip netns exec N1 ip route add 11.0.1.0/24 dev ${DEV_LEFT}
    ip netns exec N1 ip route add 11.0.2.0/24 dev ${DEV_LEFT}
    ip netns exec N1 ip route add 11.0.3.0/24 dev ${DEV_LEFT}
else
    ip netns exec N1 ip route add 1.1.3.0/24 via 1.1.2.1
    ip netns exec N1 ip route add 1.1.4.0/24 via 1.1.2.1
fi

ip netns exec N2 ip link set lo up
ip netns exec N2 ip link set veth2r up
ip netns exec N2 ip link set veth3l up
ip netns exec N2 ip addr add 1.1.2.1/24 dev veth2r
ip netns exec N2 ip addr add 1.1.3.2/24 dev veth3l
ip netns exec N2 tc qdisc add dev veth2r root tbf rate 1000mbit burst 1000kb latency 1ms
ip netns exec N2 tc qdisc add dev veth3l root tbf rate 1000mbit burst 1000kb latency 1ms
ip netns exec N2 echo 1 > /proc/sys/net/ipv4/ip_forward
if [ "${PROTOCOL}" != "direct" ]; then
    ip netns exec N2 right_${PROTOCOL} ${LOG_PREFIX} 11.0.1.4 1.1.2.2
    ip netns exec N2 left_${PROTOCOL} ${LOG_PREFIX} 11.0.2.5 1.1.3.1
    ip netns exec N2 ip route add 11.0.0.0/24 dev ${DEV_RIGHT}
    ip netns exec N2 ip route add 11.0.1.0/24 dev ${DEV_RIGHT}
    ip netns exec N2 ip route add 11.0.2.0/24 dev ${DEV_LEFT}
    ip netns exec N2 ip route add 11.0.3.0/24 dev ${DEV_LEFT}
else
    ip netns exec N2 ip route add 1.1.1.0/24 via 1.1.2.2
    ip netns exec N2 ip route add 1.1.4.0/24 via 1.1.3.1
fi

ip netns exec N3 ip link set lo up
ip netns exec N3 ip link set veth3r up
ip netns exec N3 ip link set veth4l up
ip netns exec N3 ip addr add 1.1.3.1/24 dev veth3r
ip netns exec N3 ip addr add 1.1.4.1/24 dev veth4l
ip netns exec N3 tc qdisc add dev veth3r root tbf rate 1000mbit burst 1000kb latency 1ms
ip netns exec N3 tc qdisc add dev veth4l root tbf rate 1000mbit burst 1000kb latency 1ms
ip netns exec N3 echo 1 > /proc/sys/net/ipv4/ip_forward
if [ "${PROTOCOL}" != "direct" ]; then
    ip netns exec N3 right_${PROTOCOL} ${LOG_PREFIX} 11.0.2.6 1.1.3.2
    ip netns exec N3 left_${PROTOCOL} ${LOG_PREFIX} 11.0.3.7 1.1.4.2
    ip netns exec N3 ip route add 11.0.0.0/24 dev ${DEV_RIGHT}
    ip netns exec N3 ip route add 11.0.1.0/24 dev ${DEV_RIGHT}
    ip netns exec N3 ip route add 11.0.2.0/24 dev ${DEV_RIGHT}
    ip netns exec N3 ip route add 11.0.3.0/24 dev ${DEV_LEFT}
else
    ip netns exec N3 ip route add 1.1.1.0/24 via 1.1.3.2
    ip netns exec N3 ip route add 1.1.2.0/24 via 1.1.3.2
fi

ip netns exec EP2 ip link set lo up
ip netns exec EP2 ip link set veth4r up
ip netns exec EP2 tc qdisc add dev veth4r root tbf rate 1000mbit burst 1000kb latency 1ms
ip netns exec EP2 ip addr add 1.1.4.2/24 dev veth4r
if [ "${PROTOCOL}" != "direct" ]; then
    ip netns exec EP2 right_${PROTOCOL} ${LOG_PREFIX} 11.0.3.8 1.1.4.1
    ip netns exec EP2 ip route add 0.0.0.0/0 dev ${DEV_RIGHT}
else
    ip netns exec EP2 ip route add 1.1.0.0/16 via 1.1.4.1
fi

# ip netns exec EP1 wireshark -i veth1l -k -Y ip.dst==1.1.1.2/32 &
# ip netns exec EP1 traceroute 1.1.4.2
# ip netns exec EP1 traceroute 11.0.3.8
# ip netns exec EP1 bash
# exit 0

ip netns exec ${NS_SERVER} measure_server ${LOG_PREFIX} ${IP_SERVER} &
SERVER_PID=$!

sleep 2
ip netns exec ${NS_CLIENT} measure_client ${LOG_PREFIX} ${IP_SERVER} ${TIME} &
CLIENT_PID=$!

wait ${SERVER_PID}
wait ${CLIENT_PID}
