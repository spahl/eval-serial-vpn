# Usage

```sh
sudo make setup   # Once: Under Arch Linux or Manjaro
make init         # Once: Create keys (1234, 1234, ENTER, ENTER, ENTER, yes, 1234, yes, 1234)

sudo make measure
sudo DEBUG=1 make measure
```
