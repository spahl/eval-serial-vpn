import csv
import io
import fnmatch
import seaborn as sns
import json
import os.path
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

from matplotlib.patches import Patch
from matplotlib.lines import Line2D
from collections import defaultdict
from git import Repo, Tree
from glob import glob
from scipy import stats

repo = Repo(search_parent_directories=True)

sns.set(
    font_scale=2.0,
    font='cmr10',
    style='whitegrid',
    rc={'figure.figsize':(16,10)}
)

label_order = ['LDU', 'LWU', 'LOU',
               'LDT', 'LWT', 'LOT',
               'IDU', 'IWU', 'IOU',
               'IDT', 'IWT', 'IOT']

def tag_to_commit(repo, tag_name):
    for tag in repo.tags:
        if str(tag) == tag_name:
            return tag.commit
tag_to_commit(repo, 'measurement-10min-20201127')

def get_files(glob_pattern, commit=None):
    if commit:
        for blob in commit.tree.traverse():
            if fnmatch.fnmatch(blob.path, glob_pattern):
                yield blob.path, io.StringIO(blob.data_stream.read().decode('utf-8'))
    else:
        for path in glob('../' + glob_pattern):
            yield path, open(path)

def path_to_name(path):
    name = os.path.basename(path)
    name, _ = os.path.splitext(name)
    name = name.replace('client_iperf3_', '')
    name = name.replace('server_iperf3_', '')
    name = name.replace('client_netperf_', '')
    name = name.replace('_', ' ')
    return name

def name_to_label(name):
    label = name
    label = label.replace('singlehop ', '')
    label = label.replace('threehop ', '')
    label = label.replace('internet ', 'I')
    label = label.replace('local ', 'L')
    label = label.replace('udp', 'U')
    label = label.replace('tcp', 'T')
    label = label.replace('direct ', 'D')
    label = label.replace('wg ', 'W')
    label = label.replace('openvpn tls ', 'O')
    label = label.replace('openvpn psk', 'P')
    label = label.replace('openvpn none', 'N')
    label = label.replace(' latency', '')
    return label

def name_to_protocol(name):
    if 'wg' in name:
        return '(W)ireGuard'
    if 'openvpn tls' in name:
        return '(O)penVPN'
    if 'openvpn psk' in name:
        return 'OpenVPN (P)SK'
    if 'openvpn none' in name:
        return '(N) OpenVPN (N)ONE'
    if 'direct' in name:
        return '(D)irect'
    return '(U)nkown'

def read_iperf3(glob_pattern="", commit=None):
    throughput = defaultdict(list)

    values_server = defaultdict(list)
    values_client = defaultdict(list)

    for path, file in get_files('log/{}client_iperf3_*.json'.format(glob_pattern), commit):
        name = path_to_name(path)
        data = json.load(file)

        values_client['label'].append(name_to_label(name))
        values_client['cpu'].append(data['end']['cpu_utilization_percent']['host_total'])
        if '_udp' in path:
            values_client['lost_packets'].append(data['end']['sum']['lost_packets'])
            values_client['retransmits'].append(None)
            values_client['jitter_ms'].append(data['end']['streams'][0]['udp']['jitter_ms'])
            values_client['min_rtt'].append(None)
            values_client['mean_rtt'].append(None)
            values_client['max_rtt'].append(None)
        else:
            values_client['lost_packets'].append(None)
            values_client['retransmits'].append(data['end']['sum_sent']['retransmits'])
            values_client['jitter_ms'].append(None)
            values_client['min_rtt'].append(data['end']['streams'][0]['sender']['min_rtt'])
            values_client['mean_rtt'].append(data['end']['streams'][0]['sender']['mean_rtt'])
            values_client['max_rtt'].append(data['end']['streams'][0]['sender']['max_rtt'])

        file.close()

    for path, file in get_files('log/{}server_iperf3_*.json'.format(glob_pattern), commit):
        name = path_to_name(path)
        data = json.load(file)

        if 'error' in data:
            print('error in %s' % path)
            continue
        for interval in data['intervals']:
            throughput['name'].append(name)
            throughput['label'].append(name_to_label(name))
            throughput['protocol'].append(name_to_protocol(name))
            throughput['bits_per_second'].append(interval['sum']['bits_per_second'] / 1000 / 1000)
        values_server['label'].append(name_to_label(name))
        values_server['cpu'].append(data['end']['cpu_utilization_percent']['host_total'])
        if '_udp' in path:
            values_server['lost_packets'].append(data['end']['sum']['lost_packets'])
            values_server['retransmits'].append(None)
        else:
            values_server['lost_packets'].append(None)
            values_server['retransmits'].append(None)

        file.close()

    throughput = pd.DataFrame(throughput)

    table = pd.merge(
        pd.DataFrame(values_server),
        pd.DataFrame(values_client),
        on=['label'], suffixes=['_server', '_client']
    )

    return throughput, table

def read_netperf(glob_pattern="", commit=None):
    with open('../netperf.vars') as f:
        netperf_vars = f.read()

    transactions = defaultdict(list)
    values = defaultdict(list)

    for path, file in get_files('log/{}client_netperf_*latency.csv'.format(glob_pattern), commit):
        name = path_to_name(path)
        lines = file.readlines()

        keys = [
            # 'THROUGHPUT',
            # 'THROUGHPUT_UNITS',
            # 'THROUGHPUT_CONFID',
            # 'LOCAL_SEND_THROUGHPUT',
            'TRANSACTION_RATE',
            # 'LOCAL_RECV_THROUGHPUT',
            # 'REMOTE_SEND_THROUGHPUT',
            # 'REMOTE_RECV_THROUGHPUT',
            # 'RT_LATENCY',
            'MIN_LATENCY',
            'MEAN_LATENCY',
            'MAX_LATENCY',
            # 'P50_LATENCY',
            # 'P90_LATENCY',
            # 'P99_LATENCY',
            'STDDEV_LATENCY'
        ]

        reader = csv.DictReader(io.StringIO(netperf_vars + lines[-1]))
        for row in reader:
            values['label'].append(name_to_label(name))
            for key in keys:
                values[key].append(row[key])

        reader = csv.reader(io.StringIO(''.join(lines[1:-2])))
        for row in reader:
            if row[0] == 'enable_enobufs failed: getprotobyname':
                continue
            transactions['name'].append(name)
            transactions['label'].append(name_to_label(name))
            transactions['protocol'].append(name_to_protocol(name))
            transactions['trans_per_second'].append(float(row[0]))

        file.close()


    transactions = pd.DataFrame(transactions)
    table = pd.DataFrame(values)


    return transactions, table

def plot_raw(df, column, ymin=0, ylabel='Mbit/s', yfreq=None, direct=True):
    groupped = df.groupby('name').agg(['median'])
    groupped.columns = groupped.columns.droplevel(0)
    groupped = groupped.sort_values(by='median', ascending=False)

    ax = sns.stripplot(
        x="name",
        y=column,
        order=groupped.index.get_level_values(0).tolist(),
        data=df,
        linewidth=0.2,
        size=3.0,
        zorder=0
    )
    ax = sns.boxplot(
        x="name",
        y=column,
        order=groupped.index.get_level_values(0).tolist(),
        data=df,
        linewidth=2.0,
        showfliers=False,
        zorder=10,
        dodge=False
    )

    plt.xticks(rotation=45)
    ax.set_ylim(ymin)
    ax.set(xlabel='', ylabel=ylabel)

def plot(df, column, ymin=0, ymax=None, ylabel='Mbit/s', yfreq=None, direct=True, order=label_order):
    groupped = df.groupby('label').agg(['median'])
    groupped.columns = groupped.columns.droplevel(0)
    groupped = groupped.sort_values(by='median', ascending=False)

    if order:
        groupped = groupped.reindex(order).dropna()

    ax = sns.stripplot(
        x="label",
        y=column,
        order=groupped.index.get_level_values(0).tolist(),
        data=df,
        linewidth=0,
        size=3.0,
        zorder=0,
        hue='protocol',
        hue_order=['(W)ireGuard', '(O)penVPN', '(D)irect']
    )
    ax = sns.boxplot(
        x="label",
        y=column,
        order=groupped.index.get_level_values(0).tolist(),
        data=df,
        linewidth=1.0,
        showfliers=False,
        zorder=10,
        hue='protocol',
        dodge=False,
        hue_order=['(W)ireGuard', '(O)penVPN', '(D)irect']
    )

    cmap = sns.color_palette()

    legend = [
        Patch(facecolor=cmap[2],     edgecolor='black', label='(D)irect'),
        Patch(facecolor=cmap[0],     edgecolor='black', label='(W)ireGuard'),
        Patch(facecolor=cmap[1],     edgecolor='black', label='(O)penVPN'),
        Patch(facecolor='lightgrey', edgecolor='lightgrey', alpha=0.2, label='(U)DP'),
        Patch(facecolor='white',     edgecolor='lightgrey', alpha=0.2, label='(T)CP'),
    ]

    if not direct:
        legend = legend[1:]
        plt.axvspan(-0.5, 1.5, color='lightgrey', alpha=0.2, zorder=-1)
    else:
        plt.axvspan(-0.5, 2.5, color='lightgrey', alpha=0.2, zorder=-1)
        plt.axvspan(5.5, 8.5, color='lightgrey', alpha=0.2, zorder=-1)

    ax.legend(handles=legend)
    ax.set(xlabel='', ylabel=ylabel)
    ax.set_ylim(ymin, ymax)
    if yfreq:
        ax.yaxis.set_major_locator(ticker.MultipleLocator(base=yfreq))
